package br.com.itau.investimento.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.Cliente;

import org.springframework.data.jpa.repository.JpaRepository; // Import da Class de conecção com JPA.

import org.springframework.stereotype.Repository; // Import da Class de Repository do Spring.

@Repository
public interface AplicacaoRepository extends JpaRepository<Aplicacao, Integer> {
	public Iterable<Aplicacao> findAllByCliente(Cliente cliente);
}
