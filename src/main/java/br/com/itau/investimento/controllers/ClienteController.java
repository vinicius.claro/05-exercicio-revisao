package br.com.itau.investimento.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.repositories.AplicacaoRepository;
import br.com.itau.investimento.services.AplicacaoService;
import br.com.itau.investimento.services.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

	@Autowired
	ClienteService clienteService;
	
	@Autowired
	AplicacaoService aplicacaoService;

	Cliente cliente = new Cliente();

	// consultar todos clientes
	@GetMapping
	public Iterable<Cliente> listarCliente() {
		return clienteService.obterCliente();
	}

	//consultar cliente por ID
	@GetMapping("/{id}")
	public Optional<Cliente> listarCliente(@PathVariable int id) {
		return clienteService.obterClientePorId(id);
	}

	// inserir cliente
	@PostMapping
	public void inserirCliente(@RequestBody Cliente cliente) {
		clienteService.inserirCliente(cliente);
		// return true;
	}
	
	//atualizar cliente
	@PatchMapping("/{id}")
	public ResponseEntity atualizarCliente(@PathVariable int id, @RequestBody Cliente cliente) {
		boolean resultado = clienteService.atualizar(id, cliente);

		if (!resultado) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(cliente);
	}
	//substituir cliente
	@PutMapping("/{id}")
	public ResponseEntity substituirCliente(@PathVariable int id, @RequestBody Cliente cliente) {
		boolean resultado = clienteService.substituir(id, cliente);
		
		if(!resultado) {
			return ResponseEntity.notFound().build();
		}
		
		return ResponseEntity.ok(cliente);
	}


	@DeleteMapping("/{id}")
	public ResponseEntity deletarCliente(@PathVariable int id) {
		boolean resultado = clienteService.remover(id);

		if (!resultado) {
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok().build();
	}
	
	//consultar aplicacoes do cliente
	@GetMapping("/aplic/{id}")
	public Iterable<Aplicacao> listarAplicacoes(@PathVariable int id){
		return aplicacaoService.listarAplicacoes(id);
	}

}