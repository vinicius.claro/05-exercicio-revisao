package br.com.itau.investimento.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.repositories.AplicacaoRepository;

@RestController
@RequestMapping("/aplicacao")
public class AplicacaoController {

	@Autowired
	AplicacaoRepository aplicacaoRepository;
	
	@GetMapping
	public Iterable<Aplicacao> listarAplicacoes(){
		return aplicacaoRepository.findAll();
	}
	
//	@GetMapping("/{id}/listar")
//	public Iterable<Aplicacao> listarAplicacoes(@PathVariable int id){
//		return AplicacaoService.listarAplicacoes(id);
//	}

}
