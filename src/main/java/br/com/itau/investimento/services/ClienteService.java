package br.com.itau.investimento.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.repositories.ClienteRepository;

@Service
public class ClienteService {
	@Autowired
	ClienteRepository clienteRepository;

	public Iterable<Cliente> obterCliente() {
		return clienteRepository.findAll();
	}

	public Optional<Cliente> obterClientePorId(int id) {
		return clienteRepository.findById(id);
	}
	
	public Cliente obterClientePorIdList(int id) {
		Optional<Cliente> clienteOptional = clienteRepository.findById(id);
		Cliente cliente = new Cliente(); 
		cliente = clienteOptional.get();	
		return cliente;
	}


	public boolean inserirCliente(Cliente cliente) {
		clienteRepository.save(cliente);
		return true;
	}
	
	public boolean substituir(int id, Cliente cliente) {
		Optional<Cliente> clienteOptional = clienteRepository.findById(id);

		cliente.setIdCliente(id);

		if (clienteOptional.isPresent()) {
			clienteRepository.save(cliente);
			return true;
		}

		return false;
	}

	public boolean atualizar(int id, Cliente cliente) {
		Optional<Cliente> clienteOptional = clienteRepository.findById(id);

		if (clienteOptional.isPresent()) {
			cliente = mesclarAtributos(cliente, clienteOptional.get());

			clienteRepository.save(cliente);
			return true;
		}

		return false;
	}
	public boolean remover(int id) {
		Optional<Cliente> clienteOptional = clienteRepository.findById(id);

		if (clienteOptional.isPresent()) {
			clienteRepository.delete(clienteOptional.get());
			return true;
		}

		return false;
	}

	
	private Cliente mesclarAtributos(Cliente novo, Cliente antigo) {
		if (novo.getNome() != null && !novo.getNome().isEmpty()) {
			antigo.setNome(novo.getNome());
		}

		if (novo.getEmail() != null && !novo.getEmail().isEmpty()) {
			antigo.setEmail(novo.getEmail());
		}

		// ... fazer para outros atributos atualizáveis

		return antigo;
	}
	
	



}
