package br.com.itau.investimento.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.investimento.models.Aplicacao;
import br.com.itau.investimento.models.Cliente;
import br.com.itau.investimento.repositories.AplicacaoRepository;

@Service
public class AplicacaoService {
	@Autowired
	AplicacaoRepository aplicacaoRepository;

	@Autowired
	ClienteService clienteService;

	public Iterable<Aplicacao> listarAplicacoes(int id) {

		List<Aplicacao> listaSaida = new ArrayList<Aplicacao>();
		
		Cliente cliente = clienteService.obterClientePorIdList(id);

		Iterable<Aplicacao> listaAplicacao = aplicacaoRepository.findAllByCliente(cliente);

		for (Aplicacao aplicacao : listaAplicacao) {
			listaSaida.add(aplicacao);

		}
		return listaSaida;
	}

}
